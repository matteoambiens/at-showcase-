﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace ArchToolkit.Navigation
{
    [Serializable]
    public class PathManager
    {

        public List<PathPoint> PathPoints
        {
            get
            {
                this._pathPoints.RemoveAll(go => go == null);

                foreach (var pp in GameObject.FindObjectsOfType<PathPoint>())
                {
                    if (pp == null)
                        continue;

                    if (this._pathPoints.Contains(pp))
                        continue;

                    this._pathPoints.Add(pp);
                }

                return this._pathPoints;
            }
        }
        
        [SerializeField]
        private List<PathPoint> _pathPoints = new List<PathPoint>();
   
        public void SetStartingPoint()
        {
#if UNITY_EDITOR
            
            // Check if pathlist is empty
            if (this.PathPoints != null && this.PathPoints.Count == 0)
            {
                var tom = GameObject.Instantiate(Resources.Load<GameObject>(ArchToolkitDataPaths.RESOURCES_TOM_PATH));


                Selection.activeObject = SceneView.lastActiveSceneView;
                Camera sceneCam = SceneView.lastActiveSceneView.camera;
                if (sceneCam != null)
                {
                    Vector3 spawnPos = sceneCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 5));
                    tom.transform.position = spawnPos;
                    tom.transform.rotation = Quaternion.Euler(0,sceneCam.transform.rotation.eulerAngles.y,0);
                }
                else
                    tom.transform.position = Vector3.one;

                var path = tom.AddComponent<PathPoint>();

                if (path != null)
                    path.Init(0);

                if(!this._pathPoints.Contains(path))
                    this._pathPoints.Add(path);

                Selection.activeGameObject = tom;
            }
            
            // TODO: Create entry point for adjacent points.
#else
            // TODO: Maybe we need to replace tom with arrow or point.
#endif
        }
    }
}