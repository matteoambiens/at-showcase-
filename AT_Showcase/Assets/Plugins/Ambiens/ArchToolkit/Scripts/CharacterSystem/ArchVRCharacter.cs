﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ArchToolkit.InputSystem;

namespace ArchToolkit.Character
{

    public class ArchVRCharacter : ArchCharacter
    {
        public float SafeWallThresholds = 0.25f;

        public event Action<bool> OnCheckVRInteraction;

        private Vector3[] raycastRay = new Vector3[4];

        protected override void Start()
        {
            this.SetValues();
        }

        protected override void Update()
        {
            if (this.Head == null)
            {
                Debug.LogError("Null Head");
                return;
            }

            this.VRMovement();
        }

        private void VRMovement()
        {
            if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.VR || // if we are on VR or VR mobile
                ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.VR_Mobile)
            {
              
                if (ArchToolkitManager.Instance.managerContainer.archToolkitVRManager != null &&
                    ArchToolkitManager.Instance.managerContainer.archToolkitVRManager.isVrRunning)
                {
                    if (this.raycaster != null && this.raycaster.hit.transform != null) 
                    {
                        /*if(this.raycaster.hit.transform.gameObject.layer == LayerMask.NameToLayer("UI"))
                        {

                            if (this.OnCheckVRInteraction != null)
                                this.OnCheckVRInteraction(true);

                            return;
                        }*/

                        if (this.CanTeleport(this.raycaster.hit, 45f))
                        {
                            if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.VR)
                            {
                                ArchToolkitManager.Instance.managerContainer.archToolkitVRManager.rightController.OnTriggerPressed(() =>
                                {
                                    this.Teleport(this.raycaster.hit);

                                    VR.VRFade.Instance.StartFade(this.transform.position, 0.35f, Color.black);

                                }, 0.95f);
                            }
                            else if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.VR_Mobile)
                            {
                                if(InputListener.MouseButtonLeftDown)
                                {
                                    this.Teleport(this.raycaster.hit);

                                    VR.VRFade.Instance.StartFade(this.transform.position, 0.35f, Color.black);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (this.OnCheckVRInteraction != null)
                            this.OnCheckVRInteraction(false);
                    }
                }
            }
        }

        private void Teleport(RaycastHit hit)
        {
            Vector3 dir = (this.transform.position - this.Head.transform.position).normalized;

            float dist = Vector3.Distance(this.Head.transform.position, this.transform.position);

            Vector3 finale = dir * dist;

            finale = new Vector3(finale.x, 0f, finale.z);

            this.transform.position = new Vector3(hit.point.x, hit.point.y + 0.1f, hit.point.z);

            this.transform.position += finale;
        }

        public bool CanTeleport(RaycastHit hit, float maxAngleDegree)
        {
            RaycastHit hit1;
           
            bool teletportHitCheck = true;

            if (Mathf.Abs(Vector3.Angle(hit.normal, Vector3.up)) < maxAngleDegree)
            {
                Vector3 RayOrigin = hit.point + hit.normal * SafeWallThresholds;//new Vector3 (hit.point.x, hit.point.y + SafeWallThresholds, hit.point.z);

                raycastRay[0] = new Vector3(hit.point.x + SafeWallThresholds, hit.point.y - 0.1f, hit.point.z);
                raycastRay[1] = new Vector3(hit.point.x - SafeWallThresholds, hit.point.y - 0.1f, hit.point.z);
                raycastRay[2] = new Vector3(hit.point.x, hit.point.y - 0.1f, hit.point.z + SafeWallThresholds);
                raycastRay[3] = new Vector3(hit.point.x, hit.point.y - 0.1f, hit.point.z - SafeWallThresholds);

               

                foreach (var v in raycastRay)
                {
                    if (Physics.Linecast(RayOrigin, v, out hit1))
                    {
                        if (Mathf.Abs(Vector3.Angle(hit1.normal, Vector3.up)) < maxAngleDegree)
                        {
                            //   Debug.DrawLine(hit1.point, hit1.point + hit1.normal, Color.red);
                            //  Debug.DrawLine(RayOrigin, hit1.point, Color.green);
                            
                            teletportHitCheck = true;
                        }
                        else
                        {
                            teletportHitCheck = false;
                            //   Debug.DrawLine(hit1.point, hit1.point + hit1.normal, Color.magenta);
                            //  Debug.DrawLine(RayOrigin, hit1.point, Color.white);
                        }
                    }
                    else
                    {
                        teletportHitCheck = false;
                        // Debug.DrawLine(RayOrigin, v, Color.red);
                    }
                }
            }
            else
                teletportHitCheck = false;

            if (this.OnCheckVRInteraction != null)
                this.OnCheckVRInteraction(teletportHitCheck);
            
            return teletportHitCheck;

        
        }
    }
}

