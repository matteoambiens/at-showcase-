﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ArchToolkit.InputSystem;
using UnityEngine.EventSystems;
namespace ArchToolkit.Character
{
    public class InputRaycaster : MonoBehaviour
    {
        public Camera currentCamera;

        public RaycastHit hit;

        public event Action<Transform> OnClick;

        public event Action<Transform> OnHover;

        public event Action<Transform> OnExitSensibleObject;

        public event Action OnRaycastNull;

        public bool isPointerOverUI;

        private void Update()
        {
            if (EventSystem.current != null)
            {
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    this.isPointerOverUI = true;
                    return;
                }
            }

            this.isPointerOverUI = false;

            if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.TouchOrMobile)
            {
                this.CheckRaycast(this.currentCamera.ScreenPointToRay(Input.mousePosition));
            }
            else if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.Keyboard ||
                     ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.VR_Mobile)
            {
                this.CheckRaycast(new Ray(this.currentCamera.transform.position, this.currentCamera.transform.forward));
            }
            else if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.VR && 
                     ArchToolkitManager.Instance.managerContainer.archToolkitVRManager.rightController != null)
            {
                this.CheckRaycast(new Ray(ArchToolkitManager.Instance.managerContainer.archToolkitVRManager.rightController.rayOrigin.transform.position,
                                          ArchToolkitManager.Instance.managerContainer.archToolkitVRManager.rightController.rayOrigin.transform.forward));
            }
        }

        private bool IsSensibleObject(GameObject gameObject)
        {
            return gameObject.CompareTag(ArchToolkitDataPaths.ARCHINTERACTABLETAG);
        }

        private UnityEngine.Object CheckRaycast(Ray ray)
        {
#if UNITY_EDITOR
            Debug.DrawRay(ray.origin,ray.direction,Color.blue);
#endif
            if (Physics.Raycast(ray.origin, ray.direction, out hit))
            {
                if (hit.transform != null && hit.transform.gameObject != null)
                {
                    if (this.IsSensibleObject(hit.transform.gameObject)) // if the object, is marked as interactable
                    {
                        if (this.OnHover != null)
                            this.OnHover(hit.transform);

                        if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.Keyboard ||
                            ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.VR_Mobile ||
                            ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.TouchOrMobile)
                        {
                            if (InputListener.MouseButtonLeftDown)
                            {
                                if (this.OnClick != null)
                                    this.OnClick(hit.transform);
                            }
                        }
                        else if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.VR)
                        {
                            ArchToolkitManager.Instance.managerContainer.archToolkitVRManager.rightController.OnTriggerPressed(() =>
                            {
                               if (this.OnClick != null)
                                   this.OnClick(hit.transform);
                            });
                        }
                    }
                    else
                    {

                        if (this.OnExitSensibleObject != null)
                            this.OnExitSensibleObject(hit.transform);
                    }
                }

            }
            else
            {

                if (this.OnRaycastNull != null)
                    this.OnRaycastNull();
            }
            return null;
        }

    }

}
