﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ArchToolkit.InputSystem;
using UnityEngine.XR;

namespace ArchToolkit.Character
{
    public enum MovementType
    {
        FlyCam,
        Classic
    }

    public enum LockMovementTo
    {
        None,
        FlyCam,
        Classic
    }

    public enum MobileMovementType
    {
        Joystick,
        Swipe
    }

    public class ArchCharacter : ArchCharacterBase
    {
        public Action<MovementType> OnMovementTypeChanged;
        
        public LockMovementTo LockMovement
        {
            get
            {
                return this._lockMovementTo;
            }

            set
            {
                this._lockMovementTo = value;
            }
        }

        public MovementType MovementType
        {
            get { return this._movementType; }

            set
            {
                this._movementType = value;

                if (this.OnMovementTypeChanged != null)
                    this.OnMovementTypeChanged(this._movementType);
                
            }
        }

        [Header("Input")]
        public InputRaycaster raycaster;

        [Header("Layer")]
        public LayerMask walkableLayers = 0;

        [Header("Body part")]
        public GameObject Head;

        [Space]
        [Space]

        [Header("Movement Fields")]
        
        [SerializeField]
        private float RunSpeed;

        [SerializeField]
        private float RunFaster;

        [SerializeField]
        private float movementSpeed;

        [SerializeField]
        private float clumbSpeed;
        
        [SerializeField]
        private float rotationSpeed;

        [Space]
        [Space]

        [Header("Movement Type")]
        [SerializeField]
        private MovementType _movementType = MovementType.FlyCam; // Default is Flycam
        [SerializeField]
        private MobileMovementType _mobileMovementType = MobileMovementType.Swipe;
        [SerializeField]
        private LockMovementTo _lockMovementTo = LockMovementTo.None; // Default is None
        private float rotationX = 0;
        private float rotationY = 0;
        private float initialSpeed;

        private Vector3 switchVectorPosition;
        [SerializeField]
        private Vector3 startingHeadPos;
        private Collider characterCollider;
        private Rigidbody rb;

        protected virtual void Awake()
        {
            this.SetValues();

            this.characterCollider = this.GetComponent<Collider>();

            this.rb = this.GetComponent<Rigidbody>();

            this.initialSpeed = this.movementSpeed;

            this.startingHeadPos = this.Head.transform.localPosition;

        }

        protected override void Start()
        {
            this.OnMovementTypeChanged += this.ResetHeadOnSwitch;

            if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.Keyboard)
                this.SetCursor(CursorLockMode.Locked, false);

            if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.Keyboard)
                GameObject.Instantiate(Resources.Load<GameObject>(ArchToolkitDataPaths.RESOURCES_HUD_PATH));

            else if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.TouchOrMobile)
            {
                if (this._mobileMovementType == MobileMovementType.Joystick)
                    GameObject.Instantiate(Resources.Load<GameObject>(ArchToolkitDataPaths.RESOURCES_MOBILE_HUD_JOYSTICK_PATH));
                else
                    GameObject.Instantiate(Resources.Load<GameObject>(ArchToolkitDataPaths.RESOURCES_MOBILE_HUD_SWIPE_PATH));

            }

            if (ArchToolkitManager.Instance.movementTypePerPlatform != MovementTypePerPlatform.VR)
                this.Head.transform.localRotation = Quaternion.Euler(Vector3.zero);

            this.rotationX = this.transform.eulerAngles.y;
            this.rotationY = this.transform.eulerAngles.x;
        }

        protected void SetValues()
        {
            if (!ArchToolkitManager.IsInstanced())
                ArchToolkitManager.Factory();

            this.movementSpeed = ArchToolkitManager.Instance.managerContainer.characterData.movementSpeed;
            this.RunFaster = ArchToolkitManager.Instance.managerContainer.characterData.RunFaster;
            this.RunSpeed = ArchToolkitManager.Instance.managerContainer.characterData.RunSpeed;
            this._lockMovementTo = ArchToolkitManager.Instance.managerContainer.characterData.lockMovementTo;
            this._mobileMovementType = ArchToolkitManager.Instance.managerContainer.characterData.mobileMovementType;
            this.MovementType = ArchToolkitManager.Instance.managerContainer.characterData.movementType;
            this.rotationSpeed = ArchToolkitManager.Instance.managerContainer.characterData.rotationSpeed;
            this.clumbSpeed = ArchToolkitManager.Instance.managerContainer.characterData.clumbSpeed;
            this.walkableLayers = ArchToolkitManager.Instance.managerContainer.characterData.walkableLayers;
        }


        private void ResetHeadOnSwitch(MovementType movementType)
        {
            // every time that the movement is classic, reset position to head position
            if (movementType == MovementType.Classic)
            {   
                this.characterCollider.enabled = true;
                this.Head.transform.localPosition = this.startingHeadPos;
                if(this.rb != null)
                {
                    this.rb.velocity = Vector3.zero;
                }
            }
            else
            {   this.characterCollider.enabled = false;
                if (this.rb != null)
                {
                    this.rb.velocity = Vector3.zero;
                }
            }
        }

        protected override void Update()
        {
            if (this.Head == null)
            {
                Debug.LogError("Head is null,maybe you forget to link it, in the inspector");
                return;
            }

            if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.Keyboard ||
                ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.TouchOrMobile) // If is not VR 
            {
                if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.Keyboard)
                {
                    this.CheckShift();

                    if (InputListener.MouseButtonLeftDown) // if press left mouse button, Lock mouse
                    {
                        this.SetCursor(CursorLockMode.Locked, false);
                    }
                }

                if (InputListener.SpacePressed && this._lockMovementTo == LockMovementTo.None) // if space is pressed and, the movement isn't locked
                {
                    if (this.MovementType == MovementType.FlyCam)
                        this.MovementType = MovementType.Classic;
                    else
                        this.MovementType = MovementType.FlyCam;
                }

                switch (this.MovementType)
                {
                    case MovementType.FlyCam:
                        this.FlyCamMovement();
                        break;
                    case MovementType.Classic:
                        this.ClassicMovement();
                        break;
                    default:
                        break;
                }

            } 
           

            if (Input.GetKeyDown(KeyCode.Escape))
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
                //this.SetCursor(CursorLockMode.None, true);
            }

        }
 
        private void OrbitCamera()
        {
            // TODO: Write orbit camera
        }

        private void CheckGround(Action<RaycastHit> onHit)
        {
            RaycastHit hit;

            Ray ray = new Ray(this.Head.transform.position, Vector3.down);
            Debug.DrawRay(ray.origin, ray.direction, Color.blue);
            if (Physics.Raycast(ray, out hit, 100,this.walkableLayers))
            {
                if (onHit != null)
                    onHit(hit);
            }
            else
            {
                if (onHit != null)
                    onHit(hit);
            }
        }

        private void RotateCamera(Transform transformToRotate)
        {
            if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.Keyboard)
            {
                rotationX += InputListener.MouseX * rotationSpeed * Time.deltaTime;
                rotationY -= InputListener.MouseY * rotationSpeed * Time.deltaTime;
            }
            else
            {
                if (this._mobileMovementType == MobileMovementType.Joystick)
                {
                    if (HUDJoystickMobile.RotationMobileContainer.InputDirection.magnitude > 0.55f)
                    {
                        rotationX += HUDJoystickMobile.RotationMobileContainer.InputDirection.x * rotationSpeed * Time.deltaTime;
                        rotationY -= HUDJoystickMobile.RotationMobileContainer.InputDirection.y * rotationSpeed * Time.deltaTime;
                    }
                }
                else
                {
                    rotationX += InputListener.MouseX * rotationSpeed * Time.deltaTime;
                    rotationY -= InputListener.MouseY * rotationSpeed * Time.deltaTime;
                }
            }
            
            rotationY = Mathf.Clamp(rotationY, -90, 90);

            transformToRotate.localRotation = Quaternion.Euler( new Vector3(rotationY, rotationX, 0.0f));

            //transformToRotate.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
            //transformToRotate.localRotation = Quaternion.AngleAxis(rotationY, Vector3.left);

        }

        private void RotateCameraOrBody(Transform cameraTransform, Transform bodyTransform)
        {

            if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.Keyboard)
            {
                rotationX += InputListener.MouseX * rotationSpeed * Time.deltaTime;
                rotationY -= InputListener.MouseY * rotationSpeed * Time.deltaTime;
            }
            else
            {
                if (this._mobileMovementType == MobileMovementType.Joystick)
                {
                    if (HUDJoystickMobile.MovementMobileContainer != null)
                    {
                        if (HUDJoystickMobile.RotationMobileContainer.InputDirection.magnitude > 0.55f)
                        {
                            rotationX += HUDJoystickMobile.RotationMobileContainer.InputDirection.x * rotationSpeed * Time.deltaTime;
                            rotationY -= HUDJoystickMobile.RotationMobileContainer.InputDirection.y * rotationSpeed * Time.deltaTime;
                        }
                    }
                }
                else
                {
                    rotationX += InputListener.MouseX * rotationSpeed * Time.deltaTime;
                    rotationY -= InputListener.MouseY * rotationSpeed * Time.deltaTime;
                }
            }
            rotationY = Mathf.Clamp(rotationY, -90, 90);
            
            bodyTransform.rotation = Quaternion.Euler( new Vector3(0, rotationX, 0));
            cameraTransform.localRotation = Quaternion.Euler(new Vector3(rotationY,0,0));
        }

        private void FlyCamMovement()
        {
            if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.Keyboard)
            {
                this.transform.Translate(new Vector3((InputListener.HorizontalAxis * this.movementSpeed * Time.deltaTime),
                                         0, InputListener.VerticalAxis * this.movementSpeed * Time.deltaTime),this.Head.transform);

                if (Input.GetKey(KeyCode.Q)) { this.GoUp(); }
                if (Input.GetKey(KeyCode.E)) { this.GoDown(); }
            }
            else
            {
                if (this._mobileMovementType == MobileMovementType.Joystick)
                {
                    if (HUDJoystickMobile.MovementMobileContainer != null)
                    {
                        if (HUDJoystickMobile.MovementMobileContainer.InputDirection.magnitude > 0.70f)
                        {
                            this.transform.Translate(new Vector3(HUDJoystickMobile.MovementMobileContainer.InputDirection.x * this.movementSpeed * Time.deltaTime,
                                                         0,
                                                         HUDJoystickMobile.MovementMobileContainer.InputDirection.y * this.movementSpeed * Time.deltaTime), this.Head.transform);
                        }
                    }
                }
                else
                {
                    this.transform.Translate(new Vector3(0 * this.movementSpeed * Time.deltaTime,
                                                  0, HUDSwipeMobile.TranslateVector.z * this.movementSpeed * Time.deltaTime), this.Head.transform);
                }
            }

            this.RotateCameraOrBody(this.Head.transform, this.transform);

        }

        public void GoUp()
        {
             this.transform.position += this.transform.up * this.clumbSpeed * Time.deltaTime; 
        }
        
        public void GoDown()
        {
            this.transform.position -= this.transform.up * this.clumbSpeed * Time.deltaTime;
        }

        private void ClassicMovement()
        {
            this.CheckGround( (RaycastHit hit) => 
            {
                if (hit.transform != null)
                {
                    // Force Body on the floor
                    this.transform.position = new Vector3(this.transform.position.x, hit.point.y, this.transform.position.z);
                    if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.Keyboard)
                    {
                        this.transform.Translate(new Vector3(InputListener.HorizontalAxis * this.movementSpeed * Time.deltaTime,
                                         0, InputListener.VerticalAxis * this.movementSpeed * Time.deltaTime));
                    }
                    else
                    {
                        if (this._mobileMovementType == MobileMovementType.Joystick)
                        {
                            if (HUDJoystickMobile.MovementMobileContainer != null)
                            {
                                if (HUDJoystickMobile.MovementMobileContainer.InputDirection.magnitude > 0.70f)
                                {
                                    this.transform.Translate(new Vector3(HUDJoystickMobile.MovementMobileContainer.InputDirection.x * this.movementSpeed * Time.deltaTime,
                                                                 0,
                                                                 HUDJoystickMobile.MovementMobileContainer.InputDirection.y * this.movementSpeed * Time.deltaTime));
                                }
                            }
                        }
                        else
                        {
                            this.transform.Translate(new Vector3(0 * this.movementSpeed * Time.deltaTime,
                                                     0, HUDSwipeMobile.TranslateVector.z * this.movementSpeed * Time.deltaTime));
                        }
                    }
                }
                
                else
                {
                    // Wait 2 seconds, and if the player dont fall on the ground switch the movement to flycam
                    this.StartCoroutine(this.StartTimerFalling(2, () =>
                    {
                        if (hit.transform == null)
                        {
                            this.MovementType = MovementType.FlyCam;
                        }
                    }));
                }
            });
            
            this.RotateCameraOrBody(this.Head.transform,this.transform);
        }

        private IEnumerator StartTimerFalling(float seconds, Action onTimeFinish)
        {
            transform.Translate(Vector3.down * 3 * Time.deltaTime, Space.World);
            
            yield return new WaitForSecondsRealtime(seconds);

            if (onTimeFinish != null)
                onTimeFinish();
        }

        private void CheckShift()
        {
            if (Input.GetKey(KeyCode.LeftShift))
                this.movementSpeed = this.RunSpeed + this.initialSpeed;
            else if(Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
                this.movementSpeed = this.RunFaster + this.initialSpeed;
            else
                this.movementSpeed = initialSpeed;
        }

        private void SetCursor(CursorLockMode cursorLockMode, bool visible)
        {
            Cursor.lockState = cursorLockMode;
            Cursor.visible = visible;
        }
    }
}
