﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ArchToolkit.Character
{

    public class HUDDesktop : HUD
    {
        public GameObject hud;

        protected override void Start()
        {
            base.Start();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.H) && this.hud != null)
                this.HideHUD(this.hud.activeInHierarchy);
        }

        private void HideHUD(bool active)
        {
            this.hud.SetActive(!active);
        }
    }
}