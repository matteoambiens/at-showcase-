﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using ArchToolkit.Utils.ArchMath;
using ArchToolkit.AnimationSystem;
using ArchToolkit.Utils;

namespace ArchToolkit.InputSystem
{

    public class ImageSelectedDialog : SingletonPrefab<ImageSelectedDialog>
    {
        public GameObject versionCaretLeft;
        public GameObject versionCaretRight;

        public Text title;
        public Text description;
        public GameObject wrapper;
        [SerializeField]
        public float radius = 3.0f;
        [SerializeField]
        public float minAngleToStopFollow = 3;
        [SerializeField]
        public float heightOfInterface = 2f;
        [SerializeField]
        public float maxSqrDist = 5;
        public Action onCloseCallback;
        public Action checkAction;
        public List<ArchUIButton> buttons = new List<ArchUIButton>();

        private Action<Variant> clickCallback;

        private List<Variant> itemsSelected = new List<Variant>();
        private Variant CurrentSelectedItem = null;
        private Transform lookAt;
        private int maxPages = 0;
        private int currentVersionsPage = 0;
        private const int _versionsPerPage = 3;
   
        private int currSelIndex = -1;
        private Vector3 planeNormal = Vector3.up;


        public void InitMenu(int currSelectIndex, List<Variant> elementsLogics, Action<Variant> onClick, Action onClose, Vector3 scale, Transform handle)
        {
            if (onCloseCallback != null)
                onCloseCallback();

            // Reset ALL
            this.gameObject.SetActive(true);
            this.maxPages = 0;
            this.clickCallback = null;
            this.onCloseCallback = null;


            // SET ELEMENT LOGICS
            //this.prevButton = RayHeadPointer.Instance.prevButton;
            this.itemsSelected = elementsLogics;
            this.maxPages = (int)(this.itemsSelected.Count / _versionsPerPage);
            //this.heightOfInterface = heightInterface;
            //this.minAngleToStopFollow = angleFollow;
            //
            //this.maxSqrDist = distance;
            //this.radius = radius;
            this.clickCallback = onClick;
            this.onCloseCallback = onClose;

            // SET ELEMENT GRAPHICS
            if (itemsSelected.Count >= 1)
            {
                this.title.text = itemsSelected[0].name;
                //this.itemName.text = itemsSelected[0].itemDescription;
                this.description.text = "";
            }

            this.currSelIndex = currSelectIndex;

            this.gameObject.GetComponent<RectTransform>().localScale = scale;
            this.lookAt = ArchToolkitManager.Instance.visitor.Head.transform;


            this.transform.position = new Vector3(ArchToolkitManager.Instance.visitor.Head.transform.position.x,
            ArchToolkitManager.Instance.visitor.Head.transform.position.y,
            ArchToolkitManager.Instance.visitor.Head.transform.position.z);

            if (currSelIndex == -1)
                this.currentVersionsPage = 0;
            else
                this.currentVersionsPage = (int)(currSelIndex / _versionsPerPage);

            if(ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.VR)
            {
                if (ArchToolkitManager.Instance.managerContainer.archToolkitVRManager != null)
                {
                    this.SetPositionVR();
                }
            }

            CreateButton();

        }

        void SetLastSelected()
        {
            this.title.text = itemsSelected[currSelIndex].name;
            this.description.text = itemsSelected[currSelIndex].description;

            foreach (var b in this.buttons)
            {
                if (b.selItem == itemsSelected[currSelIndex])
                {
                    b.isSelected = true;
                    break;
                }
            }
        }

        public void OnButtonClick(Variant item)
        {

            foreach (var button in this.buttons)
            {
                if (button.selItem == item)
                    button.isSelected = true;
                else
                    button.isSelected = false;
            }


            this.CurrentSelectedItem = item;

            this.description.text = CurrentSelectedItem.description;
            this.title.text = CurrentSelectedItem.name;
            if (this.clickCallback != null)
                this.clickCallback(CurrentSelectedItem);
        }

        public void Close()
        {
            this.gameObject.SetActive(false);

            if (this.onCloseCallback != null) this.onCloseCallback();
        }

        private void Update()
        {
            if (ArchToolkitManager.Instance.movementTypePerPlatform != MovementTypePerPlatform.VR)
            {
                SetPosition(Time.deltaTime * 3.0f);
            }
        }

        private void SetPositionVR()
        {
            if (ArchToolkitManager.Instance.managerContainer.archToolkitVRManager == null)
                return;

            var leftController = ArchToolkitManager.Instance.managerContainer.archToolkitVRManager.leftController;
            if (leftController != null)
            {
                this.transform.SetParent(leftController.uiAnchor.transform);
                this.transform.localPosition = new Vector3(0,0.25f,0);
                this.transform.rotation = leftController.uiAnchor.transform.rotation * Quaternion.Euler(0, 180, 0);
                this.transform.localScale = new Vector3(0.02f, 0.02f, 0.02f);
            }
        }
        
        public void ViewNextPage()
        {
            this.currentVersionsPage = Math.Min(this.currentVersionsPage + 1, maxPages);
            CreateButton();
        }

        public void ViewPrevPage()
        {
            this.currentVersionsPage = Math.Max(this.currentVersionsPage - 1, 0);
            CreateButton();
        }

        private void CheckCaret(int maxIndex)
        {
            if (this.currentVersionsPage > 0)
            {
                this.versionCaretLeft.GetComponent<Image>().enabled = true;
                this.versionCaretLeft.GetComponent<Collider>().enabled = true;
            }
            else
            {
                this.versionCaretLeft.GetComponent<Image>().enabled = false;
                this.versionCaretLeft.GetComponent<Collider>().enabled = false;
            }

            if (maxIndex < this.itemsSelected.Count)
            {
                this.versionCaretRight.GetComponent<Image>().enabled = true;
                this.versionCaretRight.GetComponent<Collider>().enabled = true;
            }
            else
            {
                this.versionCaretRight.GetComponent<Image>().enabled = false;
                this.versionCaretRight.GetComponent<Collider>().enabled = false;
            }
        }

        private void CreateButton()
        {

            if (this.itemsSelected.Count > 0)
            {
                wrapper.gameObject.SetActive(true);

                int minIndex = currentVersionsPage * _versionsPerPage;
                int maxIndex = Math.Min(minIndex + _versionsPerPage, this.itemsSelected.Count);

                //this.versionCaretLeft.SetActive((this.currentVersionsPage > 0));
                //this.versionCaretRight.SetActive((maxIndex < this.itemsSelected.Count));
                CheckCaret(maxIndex);

                foreach (Transform child in wrapper.transform)
                {
                    if (child.transform.name != versionCaretLeft.name && child.transform.name != versionCaretRight.name)
                    {
                        Destroy(child.gameObject);
                        buttons.Clear();
                    }

                }

                for (int count = minIndex; count < maxIndex; count++)
                {
                    var gO = GameObject.Instantiate(Resources.Load<GameObject>("UI/ImageSelectButton"));

                    gO.transform.SetParent(wrapper.transform);
                    gO.transform.localPosition = Vector3.zero;
                    gO.transform.localScale = Vector3.one;
                    gO.transform.localRotation = new Quaternion(0, 0, 0, 0);
                    var isb = gO.GetComponent<ArchUIButton>();
                    
                    buttons.Add(isb);

                    isb.InitButton(this.itemsSelected[count], OnButtonClick);
                }

                //this.currSelIndex = minIndex ;
                
               // if (currSelIndex != -1)
               //     SetLastSelected();

                versionCaretLeft.transform.SetAsFirstSibling();
                versionCaretRight.transform.SetAsLastSibling();
            }
            else
            {
                this.gameObject.SetActive(false);
            }
        }

        private void SetPosition(float lerp)
        {
            var vector = this.lookAt.forward;

            var distance = (
                (this.lookAt.position + this.lookAt.forward * this.radius)
                - this.transform.position).sqrMagnitude;

            Debug.DrawLine(this.lookAt.position, this.lookAt.position + this.lookAt.forward * this.radius * 2, Color.white);

            var projection = Math3d.ProjectVectorOnPlane(this.planeNormal, vector);
            var angle = Math3d.AngleVectorPlane(vector, this.planeNormal);
            if ((angle > minAngleToStopFollow || distance > this.maxSqrDist))
            {
                var finalPos = this.lookAt.position + projection * this.radius + heightOfInterface * Vector3.up;
                this.transform.LookAt(this.lookAt);
                this.transform.position = Vector3.Lerp(this.transform.position, finalPos, Time.deltaTime * 3f);
                //if (Vector3.Distance(this.transform.position, finalPos) < 0.1f)
                //{
                //    
                //}

            }
            //Debug.Log(angle);
            //Debug.DrawLine(this.lookAt.position, this.lookAt.position + projection * this.radius, Color.red);

        }
    }
}