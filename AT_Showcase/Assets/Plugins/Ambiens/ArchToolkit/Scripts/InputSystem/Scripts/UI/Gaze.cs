﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArchToolkit.InputSystem
{

    public class Gaze : ArchUIInteractable
    {
        private UnityEngine.UI.RawImage image;

        private Color startColor;

        public Color animationColor;

        [Range(0.1f,10)]
        public float animationSpeed = 0.1f;
        
        protected override void Start()
        {
            if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.TouchOrMobile)
                this.gameObject.SetActive(false);

            base.Start();

            this.image = this.gameObject.GetComponent<UnityEngine.UI.RawImage>();

            this.startColor = this.image.color;

            this.startingScale = this.gameObject.transform.localScale;
        }

        public override void OnHover(Transform hitted)
        {
            transform.localScale = Vector3.Lerp(transform.localScale,
                                                 this.hintScale,
                                                 this.animationSpeed);

            this.image.color = this.animationColor;
        }

        public override void OnExitSensibleObject(Transform hitted)
        {
            this.ResetGaze();
        }

        private void ResetGaze()
        {
            this.transform.localScale = Vector3.Lerp(this.transform.localScale,
                                               this.startingScale,
                                               this.animationSpeed);

            this.image.color = this.startColor;
        }

        public override void OnClick(Transform hitted)
        {
           
        }

        public override void OnRaycastNull()
        {
            this.ResetGaze();
        }

        public override void OnCheckVRInteract(bool canInteract)
        {
            if (canInteract)
                this.image.color = animationColor;

            else
                this.image.color = Color.red;
        }
    }
}
