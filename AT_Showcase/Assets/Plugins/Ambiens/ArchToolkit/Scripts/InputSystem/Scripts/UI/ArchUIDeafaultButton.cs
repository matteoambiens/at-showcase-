﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ArchToolkit.InputSystem
{

    public class ArchUIDeafaultButton : ArchUIInteractable
    {
        public Button button;

        public override void OnCheckVRInteract(bool canTeleport)
        {
            
        }

        public override void OnClick(Transform hitted)
        {
            if (hitted.gameObject != this.transform.gameObject)
                return;

            if (this.button != null)
                this.button.onClick.Invoke();
        }

        public override void OnExitSensibleObject(Transform hitted)
        {
            
        }

        public override void OnHover(Transform hitted)
        {
            
        }

        public override void OnRaycastNull()
        {

        }
    }
}
