﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArchToolkit.InputSystem
{
    public static class InputListener
    {
        public static bool SpacePressed
        {
            get { return Input.GetKeyDown(KeyCode.Space); }
        }

        public static bool HorizontalDown
        {
            get { return Input.GetButtonDown("Horizontal"); }
        }

        public static bool HorizontalHold
        {
            get { return Input.GetKey("Horizontal"); }
        }

        public static bool VerticalDown
        {
            get { return Input.GetKeyDown("Vertical"); }
        }

        public static bool VerticalHold
        {
            get { return Input.GetKey("Vertical"); }
        }

        public static bool MouseButtonLeftUp
        {
            get { return Input.GetMouseButtonUp(0); }
        }

        public static bool MouseButtonRightUp
        {
            get { return Input.GetMouseButtonUp(1); }
        }

        public static bool MouseButtonLeftDown
        {
            get { return Input.GetMouseButtonDown(0); }
        }

        public static bool MouseButtonLeftHold
        {
            get { return Input.GetMouseButton(0); }
        }

        public static bool MouseButtonRightDown
        {
            get { return Input.GetMouseButtonDown(1); }
        }

        public static bool MouseButtonRightHold
        {
            get { return Input.GetMouseButton(1); }
        }

        public static float MouseWheel
        {
            get { return Input.GetAxis("Mouse ScrollWheel"); }
        }

        public static float MouseX
        {
            get
            {
                if(ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.Keyboard)
                    return Input.GetAxis("Mouse X");
                else if(ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.TouchOrMobile)
                {

                    if (Input.touches.Length > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
                    {
                        return Input.GetTouch(0).deltaPosition.x;
                    }
                }

                return 0;
            }
        }

        public static float MouseY
        {
            get
            {
                if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.Keyboard)
                    return Input.GetAxis("Mouse Y");
                else if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.TouchOrMobile)
                {
                    if (Input.touches.Length > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
                    {
                        return Input.GetTouch(0).deltaPosition.y;
                    }
                }
                return 0;
            }
        }

        public static float TriggerRight
        {
            get { return Input.GetAxis("TriggerRight"); }
        }

        public static float TriggerLeft
        {
            get
            {
                return Input.GetAxis("TriggerLeft");
            }
        }

        public static float HorizontalAxis
        {
            get
            {
                return Input.GetAxis("Horizontal");
            }

        }

        public static float VerticalAxis
        {
            get
            {   
                return Input.GetAxis("Vertical");
            }
        }
    }
}
