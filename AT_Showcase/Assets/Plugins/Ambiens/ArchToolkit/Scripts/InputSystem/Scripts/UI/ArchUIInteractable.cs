﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArchToolkit.InputSystem {

    public abstract class ArchUIInteractable : MonoBehaviour
    {
        [Tooltip("This scale is used, when gaze is hover on object marked as archinteractable")]
        public Vector3 hintScale = Vector3.one;

        protected Vector3 startingScale;

        protected virtual void Start()
        {
            if(!ArchToolkitManager.IsInstanced())
            {
                Debug.LogWarning("Archtoolkit manager is not instanced");

                return;
            }

            if (ArchToolkitManager.Instance.visitor != null && ArchToolkitManager.Instance.visitor.raycaster != null)
            {
                ArchToolkitManager.Instance.visitor.raycaster.OnHover += this.OnHover;

                ArchToolkitManager.Instance.visitor.raycaster.OnExitSensibleObject += this.OnExitSensibleObject;

                ArchToolkitManager.Instance.visitor.raycaster.OnClick += this.OnClick;

                ArchToolkitManager.Instance.visitor.raycaster.OnRaycastNull += this.OnRaycastNull;

                if(ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.VR ||
                   ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.VR_Mobile)
                {
                    var visitor = ArchToolkitManager.Instance.visitor as Character.ArchVRCharacter;

                    visitor.OnCheckVRInteraction += this.OnCheckVRInteract;
                }
            }
        }

        public abstract void OnCheckVRInteract(bool canTeleport);

        public abstract void OnClick(Transform hitted);

        public abstract void OnHover(Transform hitted);

        public abstract void OnRaycastNull();

        public abstract void OnExitSensibleObject(Transform hitted);

        protected virtual void OnDestroy()
        {
            if (!ArchToolkitManager.IsInstanced () )
                return;

            if (ArchToolkitManager.Instance.visitor != null && ArchToolkitManager.Instance.visitor.raycaster != null)
            {
                ArchToolkitManager.Instance.visitor.raycaster.OnHover -= this.OnHover;

                ArchToolkitManager.Instance.visitor.raycaster.OnExitSensibleObject -= this.OnExitSensibleObject;

                ArchToolkitManager.Instance.visitor.raycaster.OnClick -= this.OnClick;
            }
        }
    }

}