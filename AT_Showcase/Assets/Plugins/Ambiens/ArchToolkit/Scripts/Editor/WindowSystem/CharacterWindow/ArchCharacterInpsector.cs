﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ArchToolkit.Navigation;
using UnityEditor;
using UnityEditor.UI;
using UnityEditor.Build;
using ArchToolkit.Character;
using UnityEngine.XR;
using UnityEditorInternal;

namespace ArchToolkit.Editor.Window
{
    internal enum VisualizationType
    {
        VR,
        Mode360
    }

    internal enum TargetNativePlatformSupported
    {
        Android,
        Ios,
        PC,
        Mac
    }

    public class ArchCharacterInpsector : ArchInspectorBase
    {
        private GameObject _selectedGameObject;

        private bool _isVisibleOnlyInSceneTab = true;

        [SerializeField]
        private TargetNativePlatformSupported targetNativePlatformSupported;

        [SerializeField]
        private VisualizationType visualizationType = VisualizationType.Mode360;


        private int tempLayerMask = 0;


        public ArchCharacterInpsector(string name) : base(name)
        {
            EditorApplication.playModeStateChanged += EditorStateChanged;

            if (EditorPrefs.HasKey("VisualizationType"))
                this.visualizationType = (VisualizationType)EditorPrefs.GetInt("VisualizationType");
            else
                EditorPrefs.SetInt("VisualizationType", (int)this.visualizationType);

            this.CheckPlatform(ref this.targetNativePlatformSupported);

            PlatformChangedListener.OnPlatformSwitched += this.PlatformSwitched;
        }

        private void EditorStateChanged(PlayModeStateChange obj)
        {
            switch (obj)
            {
                case PlayModeStateChange.EnteredEditMode:
                    break;
                case PlayModeStateChange.ExitingEditMode:
                    this.Apply(false);
                    break;
                case PlayModeStateChange.EnteredPlayMode:
                    break;
                case PlayModeStateChange.ExitingPlayMode:
                    break;
                default:
                    break;
            }
        }

        

        protected virtual void PlatformSwitched(BuildTarget buildTarget)
        {
            this.CheckPlatform(ref this.targetNativePlatformSupported, buildTarget);
        }

        public override bool IsInspectorVisible()
        {
            if (!this._isVisibleOnlyInSceneTab)
            {
                if (_selectedGameObject == null)
                {
                    this.isInspectorVisible = false;
                    return false;
                }
                var selectedPoint = _selectedGameObject.GetComponent<PathPoint>();

                if (selectedPoint != null)
                {
                    if (selectedPoint.ID == 0)
                    {
                        this.isInspectorVisible = true;
                        return true;
                    }
                }

                this.isInspectorVisible = false;
                return false;
            }
            else
            {
                if (MainArchWindow.Instance.CurrentWindow.GetStatus == WindowStatus.Scene)
                {
                    this.isInspectorVisible = true;
                    return true;
                }
            }

            this.isInspectorVisible = false;
            return false;
               
        }

        public override void OnClose()
        {
            base.OnClose();

            PlatformChangedListener.OnPlatformSwitched -= this.PlatformSwitched;

            EditorApplication.playModeStateChanged -= this.EditorStateChanged;
        }

        public override void OnEnable()
        {
            Debug.Log("Open");   
        }

        public override void OnFocus()
        {

            //this.CheckPlatform(ref this.targetNativePlatformSupported);

            this.visualizationType = (VisualizationType)EditorPrefs.GetInt("VisualizationType");
        }

        public override void OnGui()
        {
            base.OnGui();

            if (!this.inspectorFoldoutOpen)
                return;

            if (!ArchToolkitManager.IsInstanced())
            {
                GUILayout.Label("Arch toolkit manager is not instanced");

                ArchToolkitManager.Factory();
            }
            else
            {
                EditorGUILayout.LabelField("Movement Options", EditorStyles.boldLabel);

                GUILayout.Space(2);

                ArchToolkitManager.Instance.managerContainer.characterData.movementSpeed = EditorGUILayout.FloatField("Movement Speed:", ArchToolkitManager.Instance.managerContainer.characterData.movementSpeed);
                ArchToolkitManager.Instance.managerContainer.characterData.clumbSpeed = EditorGUILayout.FloatField("Clumb Speed:", ArchToolkitManager.Instance.managerContainer.characterData.clumbSpeed);
                ArchToolkitManager.Instance.managerContainer.characterData.rotationSpeed = EditorGUILayout.FloatField("Rotation Speed:", ArchToolkitManager.Instance.managerContainer.characterData.rotationSpeed);
                ArchToolkitManager.Instance.managerContainer.characterData.RunFaster = EditorGUILayout.FloatField("Run Faster:", ArchToolkitManager.Instance.managerContainer.characterData.RunFaster);
                ArchToolkitManager.Instance.managerContainer.characterData.RunSpeed = EditorGUILayout.FloatField("Run Speed:", ArchToolkitManager.Instance.managerContainer.characterData.RunSpeed);

                this.tempLayerMask = EditorGUILayout.MaskField(new GUIContent("Walkable layers: ", ArchToolkitText.WALKABLE_LAYERS_TOOLTIP), InternalEditorUtility.LayerMaskToConcatenatedLayersMask(ArchToolkitManager.Instance.managerContainer.characterData.walkableLayers.value),
                                                                                                                      InternalEditorUtility.layers);

                ArchToolkitManager.Instance.managerContainer.characterData.walkableLayers = InternalEditorUtility.ConcatenatedLayersMaskToLayerMask(this.tempLayerMask);
                this.tempLayerMask = InternalEditorUtility.ConcatenatedLayersMaskToLayerMask(this.tempLayerMask);
                ArchToolkitManager.Instance.managerContainer.characterData.walkableLayers = this.tempLayerMask;

                GUILayout.Space(2);

                EditorGUILayout.LabelField("Visualization Options", EditorStyles.boldLabel);

                GUILayout.Space(2);

                this.visualizationType = (VisualizationType)EditorGUILayout.EnumPopup("Visualization Type: ", this.visualizationType);

                if (this.visualizationType == VisualizationType.Mode360)
                {
                    if(ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.VR || // set default value if movement type is vr
                        ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.VR_Mobile)
                    {
                        if(this.targetNativePlatformSupported == TargetNativePlatformSupported.Android || 
                           this.targetNativePlatformSupported == TargetNativePlatformSupported.Ios) // if Android set to touch
                        {
                            ArchToolkitManager.Instance.movementTypePerPlatform = MovementTypePerPlatform.TouchOrMobile;
                        }
                        else
                        {
                            ArchToolkitManager.Instance.movementTypePerPlatform = MovementTypePerPlatform.Keyboard;
                        }
                    }

                    if (this.targetNativePlatformSupported != TargetNativePlatformSupported.Android &&
                       this.targetNativePlatformSupported != TargetNativePlatformSupported.Ios)
                        ArchToolkitManager.Instance.movementTypePerPlatform = (MovementTypePerPlatform)EditorGUILayout.Popup("Control Type:", (int)ArchToolkitManager.Instance.movementTypePerPlatform, new string[] { "Touch", "Keyboard" });
                    else
                    {
                        ArchToolkitManager.Instance.movementTypePerPlatform = MovementTypePerPlatform.TouchOrMobile;
                    }

                    if (ArchToolkitManager.Instance.movementTypePerPlatform == MovementTypePerPlatform.TouchOrMobile)
                    {
                        ArchToolkitManager.Instance.managerContainer.characterData.mobileMovementType = (MobileMovementType)EditorGUILayout.EnumPopup("Mobile Movement:", ArchToolkitManager.Instance.managerContainer.characterData.mobileMovementType);
                    }

                    ArchToolkitManager.Instance.managerContainer.characterData.lockMovementTo = (LockMovementTo)EditorGUILayout.EnumPopup("Lock Movement:", ArchToolkitManager.Instance.managerContainer.characterData.lockMovementTo);

                    if (ArchToolkitManager.Instance.managerContainer.characterData.lockMovementTo == LockMovementTo.None)
                    {
                        ArchToolkitManager.Instance.managerContainer.characterData.movementType = (MovementType)EditorGUILayout.EnumPopup("Movement Type:", ArchToolkitManager.Instance.managerContainer.characterData.movementType);
                    }
                }

                GUILayout.Space(2);

                EditorGUILayout.LabelField("Platform Options", EditorStyles.boldLabel);

                GUILayout.Space(2);

                this.targetNativePlatformSupported = (TargetNativePlatformSupported)EditorGUILayout.EnumPopup("Platform: ", this.targetNativePlatformSupported);

                EditorGUILayout.BeginHorizontal();
                GUI.backgroundColor = ArchToolkitWindowData.ApplyColorButton;

                if (GUILayout.Button(ArchToolkitText.APPLY))
                {
                    this.Apply();
                }

                EditorGUILayout.EndHorizontal();
            }
        }

        private void SetVR(bool enable)
        {
            if (enable && !this.IsOnMobile())
            {
                XRSettings.LoadDeviceByName("OpenVR");
            }
            else if (enable && this.IsOnMobile())
            {
                XRSettings.LoadDeviceByName("Cardboard");

                Debug.Log("Vr on mobile, can run only on the suited device, can't be used in editor mode");
            }

            if (!enable)
            {
                XRSettings.LoadDeviceByName("None");

                if(ArchToolkitManager.Instance.managerContainer.archToolkitVRManager != null)
                    GameObject.DestroyImmediate(ArchToolkitManager.Instance.managerContainer.archToolkitVRManager.gameObject);
            }


            XRSettings.enabled = enable;

            PlayerSettings.virtualRealitySupported = enable;
        }

        internal virtual void CheckPlatform(ref TargetNativePlatformSupported targetNativePlatformSupported, BuildTarget ? buildTarget = null)
        {
            if (buildTarget == null)
                buildTarget = EditorUserBuildSettings.activeBuildTarget;

            switch (buildTarget)
            {
#if UNITY_2017
                case BuildTarget.StandaloneOSXUniversal:
                    targetNativePlatformSupported = TargetNativePlatformSupported.Mac;
                    break;
#else
                case BuildTarget.StandaloneOSX:
                    targetNativePlatformSupported = TargetNativePlatformSupported.Mac;
                    break;
#endif
                case BuildTarget.StandaloneWindows:
                    targetNativePlatformSupported = TargetNativePlatformSupported.PC;
                    break;
                case BuildTarget.iOS:
                    targetNativePlatformSupported = TargetNativePlatformSupported.Ios;
                    break;
                case BuildTarget.Android:
                    targetNativePlatformSupported = TargetNativePlatformSupported.Android;
                    break;
                case BuildTarget.StandaloneWindows64:
                    targetNativePlatformSupported = TargetNativePlatformSupported.PC;
                    break;
                default:
                    break;
            }
        }

        public void Apply(bool setVr = true)
        {
            EditorPrefs.SetInt("VisualizationType", (int)this.visualizationType);

            if (this.visualizationType == VisualizationType.VR)
            {
                if (this.targetNativePlatformSupported == TargetNativePlatformSupported.Android ||
                   this.targetNativePlatformSupported == TargetNativePlatformSupported.Ios)
                    ArchToolkitManager.Instance.movementTypePerPlatform = MovementTypePerPlatform.VR_Mobile;
                else if(this.targetNativePlatformSupported == TargetNativePlatformSupported.PC ||
                        this.targetNativePlatformSupported == TargetNativePlatformSupported.Mac)
                {
                    ArchToolkitManager.Instance.movementTypePerPlatform = MovementTypePerPlatform.VR;
                }

                if(setVr)
                    this.SetVR(true);
            }

            if (this.visualizationType == VisualizationType.Mode360)
            {
                if (ArchToolkitManager.Instance.managerContainer.characterData.lockMovementTo == LockMovementTo.Classic) // Force to classic
                    ArchToolkitManager.Instance.managerContainer.characterData.movementType = MovementType.Classic;
                else if (ArchToolkitManager.Instance.managerContainer.characterData.lockMovementTo == LockMovementTo.FlyCam) // Force to flycam
                    ArchToolkitManager.Instance.managerContainer.characterData.movementType = MovementType.FlyCam;

                this.SetVR(false);
            }

            this.SwitchPlatform();
        }

        private void SwitchPlatform()
        {
            switch (this.targetNativePlatformSupported)
            {
                case TargetNativePlatformSupported.Android:
                    EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);
                    break;
                case TargetNativePlatformSupported.Ios:
                    EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.iOS, BuildTarget.iOS);
                    break;
                case TargetNativePlatformSupported.PC:
                    EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
                    break;
#if UNITY_2017
                case TargetNativePlatformSupported.Mac:
                    EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneOSXUniversal);
                    break;
#else
                case TargetNativePlatformSupported.Mac:
                    EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneOSX);
                    break;
#endif
                default:
                    break;
            }
        }

        public override void OnSelectionChange(GameObject gameObject)
        {
            this._selectedGameObject = gameObject;

            //this.CheckPlatform(ref this.targetNativePlatformSupported);
        }

        public override void OnUpdate()
        {

        }

        private bool IsOnMobile()
        {
            return (this.targetNativePlatformSupported == TargetNativePlatformSupported.Android ||
                    this.targetNativePlatformSupported == TargetNativePlatformSupported.Ios);
        }
    }
}
