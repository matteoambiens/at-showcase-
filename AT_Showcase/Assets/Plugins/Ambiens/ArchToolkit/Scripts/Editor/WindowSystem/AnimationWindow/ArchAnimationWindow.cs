﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ArchToolkit.AnimationSystem;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ArchToolkit.Editor.Window
{
    public class ArchAnimationWindow : ArchWindowCategory
    {
        public ArchAnimationWindow(WindowStatus status) : base(status)
        {
            this.ButtonCount = 3;
        }

        public override void DrawGUI()
        {
            EditorGUILayout.BeginVertical();
            
            if (GUILayout.Button(new GUIContent(ArchToolkitText.ADD_ROTATION,ArchToolkitText.ROTATION_TOOLTIP), GUILayout.Height(ArchToolkitWindowData.BUTTON_HEIGHT)))
            {
                ArchDoorInspector.Instance.AddAnimation<RotateAround>();
            }

            if (GUILayout.Button(new GUIContent(ArchToolkitText.ADD_TRANSLATION,ArchToolkitText.TRANSLATION_TOOLTIP), GUILayout.Height(ArchToolkitWindowData.BUTTON_HEIGHT)))
            {
                ArchDrawerInspector.Instance.AddAnimation<Translate>();
            }

            if(GUILayout.Button(new GUIContent( ArchToolkitText.ADD_SWITCH_MATERIAL,ArchToolkitText.SWITCH_MATERIAL_TOOLTIP), GUILayout.Height(ArchToolkitWindowData.BUTTON_HEIGHT)))
            {
                ArchMaterialInspector.Instance.SetAnimation(Selection.activeGameObject);
            }
          
            EditorGUILayout.EndVertical();
        }

        public override void OnClose()
        {
            
        }

        public override void OnSelectionChange(GameObject gameObject)
        {
            
        }
    }
}
