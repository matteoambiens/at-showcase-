﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ArchToolkit.Utils;
using UnityEditor;

namespace ArchToolkit.Editor.Window
{

    public class ArchPhysicsWindow : ArchWindowCategory
    {
        private GameObject selected;

        public ArchPhysicsWindow(WindowStatus windowStatus) : base(windowStatus)
        {
            this.ButtonCount = 1;
        }

        public override void DrawGUI()
        {
            EditorGUILayout.BeginVertical();

            EditorGUI.BeginDisabledGroup(this.selected == null || this.selected.GetComponent<MeshFilter>() == null || 
                                         this.selected.GetComponent<MeshFilter>().sharedMesh == null ||
                                         this.selected.GetComponent<Collider>() );

            if (GUILayout.Button(new GUIContent("Add Block Property", ArchToolkitText.ADD_COLLIDER_TOOLTIP), GUILayout.Height(ArchToolkitWindowData.BUTTON_HEIGHT)))
            {
                // var bounds =  ArchToolkitProgrammingUtils.GetBounds(this.selected);
                this.selected.AddComponent<MeshCollider>();
                // bC.center = bounds.center - this.selected.transform.position;
                // bC.size = bounds.size;
            }

            EditorGUI.EndDisabledGroup();

            EditorGUILayout.EndVertical();

        }

        public override void OnClose()
        {
            
        }

        public override void OnOpen()
        {
            
        }

        public override void OnSelectionChange(GameObject gameObject)
        {
            this.selected = gameObject;
            
        }
    }
}
