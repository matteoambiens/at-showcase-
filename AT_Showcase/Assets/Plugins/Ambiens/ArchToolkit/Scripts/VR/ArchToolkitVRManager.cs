﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace ArchToolkit.VR
{
    public enum ConnectedDevice
    {
        Not_Established,
        None,
        Htc_Vive,
        Oculus_Rift,
        Dell_Visor,
        Mobile_Cardboard
    }

    public enum JoystickConnected
    {
        Not_Established,
        Htc_Vive_Controller,
        Windows_Mixed_Controller,
        Oculus_Rift_Controller,
        Open_VR_Controller
    }

    public class ArchToolkitVRManager : MonoBehaviour
    {
        public ConnectedDevice ConnectedDevice
        {
            get
            {
                return this._connectedDevice;
            }
            protected set
            {
                this._connectedDevice = value;
            }
        }

        public bool isDeviceConnected;

        public bool isVrRunning;

        [SerializeField]
        public List<JoystickConnected> joysticksConnected = new List<JoystickConnected>();
        [SerializeField]
        private ConnectedDevice _connectedDevice = ConnectedDevice.Not_Established;

        public ArchVRControllerBase leftController;

        public ArchVRControllerBase rightController;

        public bool isTrackingAcquired = false;

        //public ArchVRControllerBase currentUsedController;

        protected virtual void Awake()
        {
            InputTracking.trackingLost += InputTracking_trackingLost;

            InputTracking.trackingAcquired += InputTracking_trackingAcquired;

            this.SetDevice();

            this.SetJoysticks();
        }

        private void OnDestroy()
        {
            InputTracking.trackingLost -= InputTracking_trackingLost;

            InputTracking.trackingAcquired -= InputTracking_trackingAcquired;

            this.joysticksConnected.Clear();
        }

        private void OnApplicationQuit()
        {
            InputTracking.trackingLost -= InputTracking_trackingLost;

            InputTracking.trackingAcquired -= InputTracking_trackingAcquired;
        }

        private void InputTracking_trackingAcquired(XRNodeState obj)
        {
            if (obj.nodeType == XRNode.Head)
                this.isTrackingAcquired = true;
            if(obj.nodeType == XRNode.LeftHand)
            {
                if (this.leftController != null)
                    this.leftController.trackingAcquired = true;
            }
            if (obj.nodeType == XRNode.RightHand)
            {
                if (this.rightController != null)
                    this.rightController.trackingAcquired = true;
            }
        }

        private void InputTracking_trackingLost(XRNodeState obj)
        {
            if (obj.nodeType == XRNode.Head)
                this.isTrackingAcquired = false;

            if (obj.nodeType == XRNode.LeftHand)
            {
                if (this.leftController != null)
                    this.leftController.trackingAcquired = false;
            }
            if (obj.nodeType == XRNode.RightHand)
            {
                if (this.rightController != null)
                    this.rightController.trackingAcquired = false;
            }
        }

        protected virtual void Update()
        {
            if(this.ConnectedDevice == ConnectedDevice.None ||
               this.ConnectedDevice == ConnectedDevice.Not_Established)
            {
                this.SetDevice();
                return;
            }

            this.isDeviceConnected = XRDevice.isPresent;

            this.isVrRunning = XRSettings.isDeviceActive;

            Debug.Log("Vr is running " + this.isVrRunning);
        }

        public virtual string GetCurrentDevice()
        {
            string device = "None";

            if (XRDevice.isPresent)
                device = XRDevice.model;

#if UNITY_EDITOR
            Debug.Log("Current Device is " + device);
#endif
            return device;
        }

        public virtual void SetJoysticks()
        {
            if (this.ConnectedDevice == ConnectedDevice.Not_Established ||
                this.ConnectedDevice == ConnectedDevice.None)
                return;

            var joysticks = Input.GetJoystickNames();

            if (joysticks == null)
                this.joysticksConnected.Clear();

            if (joysticks != null && joysticks.Length == 0)
                this.joysticksConnected.Clear();


            foreach (var pad in joysticks)
            {
                if (string.IsNullOrEmpty(pad))
                    continue;

                switch (this._connectedDevice)
                {
                    case ConnectedDevice.Not_Established:
                        this.joysticksConnected.Add(JoystickConnected.Open_VR_Controller);
                        break;
                    case ConnectedDevice.None:
                        break;
                    case ConnectedDevice.Htc_Vive:
                        this.joysticksConnected.Add(JoystickConnected.Htc_Vive_Controller);
                        break;
                    case ConnectedDevice.Oculus_Rift:
                        this.joysticksConnected.Add(JoystickConnected.Oculus_Rift_Controller);
                        break;
                    case ConnectedDevice.Dell_Visor:
                        this.joysticksConnected.Add(JoystickConnected.Windows_Mixed_Controller);
                        break;
                    case ConnectedDevice.Mobile_Cardboard:
                        break;
                    default:
                        break;
                }

            }
        }

        protected virtual void SetDevice()
        {
            var device = this.GetCurrentDevice();

            if (device.Contains("DELL VISOR"))
                this.ConnectedDevice = ConnectedDevice.Dell_Visor;
            else if (device.Contains("Cardboard"))
                this.ConnectedDevice = ConnectedDevice.Mobile_Cardboard;
            else if (device.Contains("Vive"))
                this.ConnectedDevice = ConnectedDevice.Htc_Vive;
            else if (device.Contains("Oculus"))
                this.ConnectedDevice = ConnectedDevice.Oculus_Rift;
            else if (device == "None")
                this.ConnectedDevice = ConnectedDevice.None;
            else
                this.ConnectedDevice = ConnectedDevice.Not_Established;

        }

    }
}