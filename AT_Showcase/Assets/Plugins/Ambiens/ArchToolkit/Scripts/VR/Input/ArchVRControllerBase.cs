﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ArchToolkit.Utils;
using System;
using UnityEngine.SpatialTracking;
using UnityEngine.XR;
using ArchToolkit.InputSystem;

namespace ArchToolkit.VR
{
    [RequireComponent(typeof(TrackedPoseDriver))]
    public class ArchVRControllerBase : MonoBehaviour
    {
        [SerializeField]
        public XRNode NodeType;

        public bool trackingAcquired = false;

        [SerializeField]
        private bool triggerPressed;

        public RaycastHit hit;

        public LineRenderer lineRenderer;

        public GameObject uiAnchor;

        public GameObject rayOrigin;

        protected virtual void Start()
        {
            this.InitializeController();
        }

        protected virtual void Update()
        {
            transform.localPosition = InputTracking.GetLocalPosition(NodeType);
            transform.localRotation = InputTracking.GetLocalRotation(NodeType);
            this.SetLine();
        }

        internal virtual void SetController(Action onError)
        {
            switch (ArchToolkitManager.Instance.managerContainer.archToolkitVRManager.ConnectedDevice)
            {
                case ConnectedDevice.Not_Established:
                    //Load Oculus by default
                    if (this.NodeType == XRNode.LeftHand)
                        this.InstantiateController(ArchToolkitDataPaths.RESOURCES_OCULUS_CONTROLLER_LEFT, onError);
                    else if (this.NodeType == XRNode.RightHand)
                        this.InstantiateController(ArchToolkitDataPaths.RESOURCES_OCULUS_CONTROLLER_RIGHT, onError);
                    break;
                case ConnectedDevice.None:
                    break;
                case ConnectedDevice.Htc_Vive:
                    this.InstantiateController(ArchToolkitDataPaths.RESOURCES_VIVE_CONTROLLER, onError);
                    break;
                case ConnectedDevice.Oculus_Rift:
                    if (this.NodeType == XRNode.LeftHand)
                        this.InstantiateController(ArchToolkitDataPaths.RESOURCES_OCULUS_CONTROLLER_LEFT, onError);
                    else if (this.NodeType == XRNode.RightHand)
                        this.InstantiateController(ArchToolkitDataPaths.RESOURCES_OCULUS_CONTROLLER_RIGHT, onError);
                    break;
                case ConnectedDevice.Dell_Visor:
                    if (this.NodeType == XRNode.LeftHand)
                        this.InstantiateController(ArchToolkitDataPaths.RESOURCES_WMX_CONTROLLER_LEFT, onError);
                    else if (this.NodeType == XRNode.RightHand)
                        this.InstantiateController(ArchToolkitDataPaths.RESOURCES_WMX_CONTROLLER_RIGHT,onError);
                    break;
                case ConnectedDevice.Mobile_Cardboard:
                    break;
                default:
                    break;
            }
        }

        protected virtual void InstantiateController(string path,Action onError)
        {
            try
            {
                var obj = GameObject.Instantiate(Resources.Load<GameObject>(path), Vector3.zero, Quaternion.identity, this.transform);
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localRotation = Quaternion.Euler(Vector3.zero);

            }
            catch (Exception e)
            {
                if (onError != null)
                    onError();

                Debug.LogError("Attention: " + e.Message);
            }
            
        }

        protected virtual void CreateUIAnchor()
        {
            //Ui
            uiAnchor = new GameObject("UiAnchor");

            uiAnchor.transform.SetParent(this.transform);

            uiAnchor.transform.localPosition = Vector3.zero;
            uiAnchor.transform.localPosition = new Vector3(
                uiAnchor.transform.localPosition.x,
                 0.05f,
                0.08f
                );

            uiAnchor.transform.localRotation = Quaternion.Euler(Vector3.zero) ;
        }

        protected virtual bool RayCastForLine(Ray ray)
        {
#if UNITY_EDITOR
            Debug.DrawRay(ray.origin, ray.direction, Color.red);
#endif
            return Physics.Raycast(ray.origin, ray.direction, out hit);
        }

        protected virtual void SetLine()
        {
            if (this.lineRenderer != null)
            {
                this.lineRenderer.enabled = true;

                if (this.RayCastForLine(new Ray(this.rayOrigin.transform.position, this.rayOrigin.transform.forward)))
                {
                    ArchGazeCanvas.Instance.SetTransform(hit.point, hit.normal, (this.transform.position - hit.point).magnitude);

                    this.lineRenderer.SetPosition(0, this.rayOrigin.transform.position);
                    this.lineRenderer.SetPosition(1, ArchGazeCanvas.Instance.transform.position);
                }
                else
                {
                    ArchGazeCanvas.Instance.SetTransform(this.rayOrigin.transform.position + this.rayOrigin.transform.forward * 20, this.rayOrigin.transform.forward, 20);

                    this.lineRenderer.SetPosition(0, this.rayOrigin.transform.position);
                    this.lineRenderer.SetPosition(1, ArchGazeCanvas.Instance.transform.position);
                }
            }
        }

        public virtual void OnControllerMoving()
        {

        }

        public virtual void OnTriggerPressed(Action OnClick, float triggerSensibility = 0.85f)
        {
            if (this.NodeType == XRNode.LeftHand)
            {
                if (!this.triggerPressed && InputListener.TriggerLeft >= triggerSensibility)
                {
                    this.triggerPressed = true;

                    if (OnClick != null)
                        OnClick();
                }
                else
                {
                    if (this.triggerPressed && InputListener.TriggerLeft < triggerSensibility)
                    {
                        this.triggerPressed = false;
                    }
                }
            }

            else if (this.NodeType == XRNode.RightHand)
            {
                if (!this.triggerPressed && InputListener.TriggerRight >= triggerSensibility)
                {
                    this.triggerPressed = true;

                    if (OnClick != null)
                        OnClick();
                }
                else
                {
                    if (this.triggerPressed && InputListener.TriggerRight < triggerSensibility)
                    {
                        this.triggerPressed = false;
                    }
                }
            }
        }

        public virtual void OnMenuPressed()
        {

        }

        public virtual void VibrateController()
        {

        }

        internal virtual void InitializeController()
        {
            if (ArchToolkitManager.Instance.managerContainer.archToolkitVRManager == null)
            {
                Debug.LogWarning("VR manager is not instanced");
                return;
            }

            if (this.NodeType == XRNode.LeftHand)
            {
                this.GetComponent<TrackedPoseDriver>().SetPoseSource(TrackedPoseDriver.DeviceType.GenericXRController, TrackedPoseDriver.TrackedPose.LeftPose);
                ArchToolkitManager.Instance.managerContainer.archToolkitVRManager.leftController = this;
                this.CreateUIAnchor();
            }
            else if (this.NodeType == XRNode.RightHand)
            {
                this.GetComponent<TrackedPoseDriver>().SetPoseSource(TrackedPoseDriver.DeviceType.GenericXRController, TrackedPoseDriver.TrackedPose.RightPose);
                ArchToolkitManager.Instance.managerContainer.archToolkitVRManager.rightController = this;
            }

            this.SetController(null);


        }
    }
}