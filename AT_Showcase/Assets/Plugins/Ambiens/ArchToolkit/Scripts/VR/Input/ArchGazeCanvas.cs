﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArchToolkit.Utils;

namespace ArchToolkit.VR
{

    public class ArchGazeCanvas : Singleton<ArchGazeCanvas>
    {
        public bool influenceScale;

        private Vector3 startScale = Vector3.one;
        private Quaternion targetRotation = Quaternion.identity;

        void Awake()
        {
            this.startScale = this.transform.localScale;
        }

        void Update()
        {
            if (this.transform.rotation != targetRotation)
            {
                this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetRotation, Time.deltaTime * 10f);
            }



        }

        public void SetTransform(Vector3 hitPosition, Vector3 hitNormal, float distance)
        {
            this.transform.position = hitPosition;
            this.targetRotation = Quaternion.FromToRotation(Vector3.forward, hitNormal);
            if (this.influenceScale)
                this.transform.localScale = startScale * distance;
        }
    }
}
