﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ArchToolkit.ArchGizmos;


namespace ArchToolkit.AnimationSystem
{
    [ExecuteInEditMode]
    public class ArchBasicHandle : MonoBehaviour
    {
        public AInteractable animationToOpen;

        private void Start()
        {
            if (ArchToolkitManager.IsInstanced())
            {
                if (ArchToolkitManager.Instance.visitor != null && ArchToolkitManager.Instance.visitor.raycaster != null)
                    ArchToolkitManager.Instance.visitor.raycaster.OnClick += this.OnClick;
            }
#if UNITY_EDITOR
            if (!UnityEditor.EditorApplication.isPlaying && !UnityEditor.EditorApplication.isCompiling)
            {
                if (this.animationToOpen != null)
                    this.animationToOpen.onDestroy += this.AnimationDestroyed;
            }
#endif
        }

        public void OnClick(Transform transform)
        {
            if (transform.gameObject != this.gameObject)
            {
                return;
            }

            if (this.animationToOpen != null)
            {
                if (this.animationToOpen.StartWith != AInteractable.StartWithType.waitcall)
                    this.animationToOpen.StartAnimation();
            }
        }

        private void Update()
        {
#if UNITY_EDITOR
            if (this.animationToOpen == null)
                GameObject.DestroyImmediate(this);
#endif
        }

        private void AnimationDestroyed()
        {
           // GameObject.DestroyImmediate(this);
        }
    }
}