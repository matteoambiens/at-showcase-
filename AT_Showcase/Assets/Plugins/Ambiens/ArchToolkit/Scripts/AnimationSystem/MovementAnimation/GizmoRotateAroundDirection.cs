﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArchToolkit.AnimationSystem
{

    [ExecuteInEditMode][System.Serializable]
    public class GizmoRotateAroundDirection : MonoBehaviour
    {
        private Mesh mesh;

        [SerializeField][HideInInspector]
        private Translate translate;

        [SerializeField][HideInInspector]
        private RotateAround rotate;

        private void Start()
        {
            GameObject app;
            if (this.transform.GetComponentInParent<Translate>())
            {
                app = GameObject.Instantiate(Resources.Load(ArchToolkitDataPaths.RESOURCESPATH_TRANSLATE_ARROW)) as GameObject;
            }
            else
            {
                app = GameObject.Instantiate(Resources.Load(ArchToolkitDataPaths.RESOURCEPATH_ROTATION_ARROW)) as GameObject;
            }

            mesh = app.GetComponentInChildren<MeshFilter>().sharedMesh;
            DestroyImmediate(app);

            this.rotate = this.transform.GetComponentInParent<RotateAround>();
            this.translate = this.transform.GetComponentInParent<Translate>();
        }

        Vector3 dir;

        void OnDrawGizmos()
        {
            //var rotationMatrix = Matrix4x4.TRS(this.transform.position,this.transform.rotation, Vector3.one);

            var rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
            Gizmos.matrix = rotationMatrix;
            // Gizmos.color = new Color32(238, 233, 66, 200);
            Gizmos.color = Color.red;

            Gizmos.DrawMesh(mesh, Vector3.zero, Quaternion.identity, new Vector3(0.1f, 0.1f, 0.1f));

            var pivot = transform.parent.Find("pivot");

            if (pivot == null)
            {
                Debug.LogWarning("Gizmo pivot not found");
                return;
            }

            if (this.translate != null)
            {
                 
                dir = pivot.transform.position - this.transform.position;
                transform.rotation = Quaternion.LookRotation(-dir * -180);
            }
            else if (rotate != null)
            {
                dir = pivot.transform.position - this.transform.position;

                transform.rotation = Quaternion.LookRotation(dir * rotate.animationAmount);
            }
        }
    }
}