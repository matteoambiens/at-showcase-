﻿using UnityEngine;
using System;
using ArchToolkit.Navigation;
using ArchToolkit.Character;
using ArchToolkit.Utils;
using ArchToolkit.VR;
using UnityEngine.EventSystems;

namespace ArchToolkit
{
    public enum MovementTypePerPlatform
    {
        TouchOrMobile,
        Keyboard,
        VR,
        VR_Mobile
    }

    [Serializable]
    public class ArchToolkitCharacterData
    {
        public MovementType movementType = MovementType.FlyCam; // Default is Flycam
        
        public MobileMovementType mobileMovementType = MobileMovementType.Swipe;
        
        public LockMovementTo lockMovementTo = LockMovementTo.None; // Default is None

        public LayerMask walkableLayers = 1;

        public float RunSpeed = 4.5f;

        public float RunFaster = 15;

        public float movementSpeed = 2;

        public float clumbSpeed = 4;

        public float rotationSpeed = 40;
        
    }

    [Serializable]
    public class ArchToolkitManager : Singleton<ArchToolkitManager>
    {
        [Serializable]
        public struct ManagersContainer
        {
            public PathManager pathManager;

            public ArchToolkitCharacterData characterData;

            public ArchToolkitVRManager archToolkitVRManager;
        }

        public ManagersContainer managerContainer = new ManagersContainer();

        //[HideInInspector]
        public MovementTypePerPlatform movementTypePerPlatform = MovementTypePerPlatform.Keyboard;

        public Action OnVisitorCreated;

        public ArchCharacter visitor;

        public Transform Tom;

        [SerializeField]
        private EventSystem eventSystem;

        public static ArchToolkitManager Factory()
        {
            if (!ArchToolkitManager.IsInstanced())
            {
                var archManager = new GameObject("ArchToolkitManager DON'T DESTROY THIS");

                ArchToolkitManager._instance = archManager.AddComponent<ArchToolkitManager>();
            }

            _instance.Init();

            return _instance;

        }

        private void Init()
        {
            if (this.managerContainer.pathManager == null)
            {
                this.managerContainer.pathManager = new PathManager();
            }

            if (this.managerContainer.characterData == null)
                this.managerContainer.characterData = new ArchToolkitCharacterData();
            
            if ((this.movementTypePerPlatform == MovementTypePerPlatform.VR || this.movementTypePerPlatform == MovementTypePerPlatform.VR_Mobile) &&
                this.managerContainer.archToolkitVRManager == null)
            {

                var archVR = GameObject.FindObjectOfType<ArchToolkitVRManager>();

                if (archVR == null)
                {
                    var goVR = new GameObject("ArchToolkitVRManager");

                    archVR = goVR.AddComponent<ArchToolkitVRManager>();
                }

                archVR.gameObject.transform.SetParent(this.transform);

                this.managerContainer.archToolkitVRManager = archVR;

            }

            if (this.eventSystem == null)
            {
                var eventSystemGO = GameObject.Instantiate(Resources.Load<GameObject>(ArchToolkitDataPaths.RESOURCES_EVENTSYSTEM));

                eventSystemGO.transform.SetParent(this.transform);

                this.eventSystem = eventSystemGO.GetComponent<EventSystem>();
            }
        }

        private void Awake()
        {
            this.Init();

            if (this.managerContainer.pathManager.PathPoints.Count == 0 || this.managerContainer.pathManager.PathPoints[0] == null)
            {
                return;
            }

            var startPoint = this.managerContainer.pathManager.PathPoints[0];

            if (startPoint != null)
            {
                this.Tom = startPoint.transform;
            }

            if(this.Tom == null)
            {
                Debug.LogError("Tom is null, please create your starting point");
                return;
            }

            GameObject character = null;
            if (this.managerContainer.archToolkitVRManager == null)
            {
                character = GameObject.Instantiate(Resources.Load<GameObject>(ArchToolkitDataPaths.RESOURCES_CHARACTER_PREFAB),this.Tom.transform.position,this.Tom.transform.rotation);
            }
            else
            {
                if (this.movementTypePerPlatform == MovementTypePerPlatform.VR)
                {
                    if (this.managerContainer.archToolkitVRManager.ConnectedDevice != ConnectedDevice.None ||
                       this.managerContainer.archToolkitVRManager.ConnectedDevice != ConnectedDevice.Not_Established)
                    {
                        character = GameObject.Instantiate(Resources.Load<GameObject>(ArchToolkitDataPaths.RESOURCES_CHARACTER_VR_PREFAB),this.Tom.transform.position,this.Tom.transform.rotation);
                    }
                }
                else if(this.movementTypePerPlatform == MovementTypePerPlatform.VR_Mobile)
                {
                    character = GameObject.Instantiate(Resources.Load<GameObject>(ArchToolkitDataPaths.RESOURCES_CHARACTER_MOBILE_VR_PREFAB),this.Tom.transform.position,this.Tom.transform.rotation);
                }
            }

            if(character == null)
            {
                Debug.LogError("Cannot instantiate visitor,something is gone wrong");
                return;
            } 

            this.visitor = character.GetComponent<ArchCharacter>();

            if (this.OnVisitorCreated != null)
                this.OnVisitorCreated();

            DontDestroyOnLoad(this);

        }
    }
}
